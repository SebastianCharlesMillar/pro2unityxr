﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class CameraLook : MonoBehaviour
{
    public float _y;
    public float _x;
    public float _speed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _x += Input.GetAxis("Mouse Y");
        _y += -Input.GetAxis("Mouse X");

        transform.eulerAngles = new Vector3(_x, _y, 0) * _speed;



    }
}
